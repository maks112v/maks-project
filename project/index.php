<html lang="en">
  <head>
    <title>Hello, world!</title>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
		<link rel="stylesheet" href="assets/css/main.css">
  </head>
  <body>
    <h1>Hello, world!</h1>
    <div class="card">
      <img src="assets/img/img_avatar.png" style="width:100%">
      <div class="container">
      <h4><b>John Doe</b></h4>
      <p>Software Engineer</p>
      </div>
  </body>
</html>