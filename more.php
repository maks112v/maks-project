<html>

<head>
	<title>How to make and host a simple web page</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	 crossorigin="anonymous">
	<link rel="stylesheet" href="https://getbootstrap.com/docs/4.1/assets/css/docs.min.css">
	<style>
	.bd-navbar {
    background-color: #2196F3 !important;
	}
	</style>
</head>

<body>

	<header class="navbar navbar-expand-lg navbar-light flex-column flex-md-row bd-navbar">
		<div class="container">
			<a class="navbar-brand text-light" href="/">Maks Project</a>

			<div class="navbar-nav-scroll">
				<ul class="navbar-nav bd-navbar-nav flex-row">
					<li class="nav-item">
						<a class="nav-link " href="/">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" href="/more.php">More</a>
					</li>
			</div>
	</header>


<div class="container-fluid">
<div class="row flex-xl-nowrap">


<div class=" d-xl-block col-md-3 col-xl-2 bd-toc">
	<ul class="section-nav">
		<li class="toc-entry toc-h2"><a href="#prep-env">Preparing Environment</a></li>
		<li class="toc-entry toc-h2"><a href="#file-structure">File Structure</a></li>
		<li class="toc-entry toc-h2"><a href="#build-web">Build Website</a>
			<ul>
				<li class="toc-entry toc-h3"><a href="#add-html">Adding HTML</a></li>
				<li class="toc-entry toc-h3"><a href="#link-css">Link CSS</a></li>
				<li class="toc-entry toc-h3"><a href="#add-css">Adding CSS</a></li>
			</ul>
		</li>
		<li class="toc-entry toc-h2"><a href="#host-web">Hosting Website</a>
			<ul>
				<li class="toc-entry toc-h3"><a href="#prep-ubuntu">Preparing Ubuntu</a></li>
				<li class="toc-entry toc-h3"><a href="#add-site">Add Website</a></li>
			</ul>
		</li>
		<li class="toc-entry toc-h2"><a href="#final">Final Website</a>
	</ul>
</div>


<main class="col-12 col-md-9 col-xl-10 py-md-3 pl-md-5 bd-content" role="main">
	<h1 class="bd-title" id="content">Create and Host Simple Website.</h1>
	<p class="bd-lead">How to create and host a simple website using only HTML, CSS, Javascript, PHP, and Apache</p>
	<h2 id="prep-env">Preparing Environment</h2>
	<p>To develop a website it is easiest to be able to see it. To not have to install php on your computer and create a local virtual host we will use <a href="https://codeanywhere.com/">Code Anywhere</a> free container. After signing up and entering the IDE go to File > New Connection > Container. Give the container a name and select PHP - Ubuntu 16.04 from the stacks then hit create.</p>
	<h2 id="file-structure">File Structure</h2>
	<p>To make the project easy to work with you need an understandable file structure. For this project we will use this file structure.</p>
	<figure class="highlight">
		<pre><code class="language-plaintext" data-lang="plaintext">project/
├── assets/
│   ├── css/
│   │	└── main.css
│   └── img/
│       └── img-avatar.png
├── index.php
└── about.php</code></pre>
	</figure>
	<p>Alternatively, you may make the css and javascript inline with the index.php file:</p>
	<figure class="highlight">
		<pre><code class="language-html" data-lang="html"><span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">"en"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;head&gt;</span>
    <span class="c">&lt;!-- CSS Goes Here --&gt;</span>
    <span class="nt">&lt;style&gt;</span>
	
    <span class="nt">&lt;/style&gt;</span>
  <span class="nt">&lt;/head&gt;</span>
  <span class="nt">&lt;body&gt;</span>

    <span class="c">&lt;!-- JS Goes Here --&gt;</span>
    <span class="nt">&lt;script&gt;</span>
		
    <span class="nt">&lt;/script&gt;</span>
  <span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span></code></pre></figure>
	
	<p>This is not recommended because your website will load slower and the work environment will be cluttered.</p>
	
	<h2 id="build-web">Build Website</h2>
	<h3 id="add-html">Adding HTML</h3>
	<p>At this point add some html to the website to get some content to work with by makeing a simple card design.</p>
<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;html</span> <span class="na">lang=</span><span class="s">"en"</span><span class="nt">&gt;</span>
  <span class="nt">&lt;head&gt;</span>
    <span class="nt">&lt;title&gt;</span>Hello, world!<span class="nt">&lt;/title&gt;</span>
  <span class="nt">&lt;/head&gt;</span>
  <span class="nt">&lt;body&gt;</span>
    <span class="nt">&lt;h1&gt;</span>Hello, world!<span class="nt">&lt;/h1&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"card"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;img</span> <span class="na">src=</span><span class="s">"assets/img/img_avatar.png"</span> <span class="na">style=</span><span class="s">"width:100%"</span><span class="nt">&gt;</span>
      <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"container"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;h4&gt;</span><span class="nt">&lt;b&gt;</span>John Doe<span class="nt">&lt;/b&gt;</span><span class="nt">&lt;/h4&gt;</span>
        <span class="nt">&lt;p&gt;</span>Software Engineer<span class="nt">&lt;/p&gt;</span>
      <span class="nt">&lt;/div&gt;</span>
  <span class="nt">&lt;/body&gt;</span>
<span class="nt">&lt;/html&gt;</span></code></pre></figure>
	<h3 id="link-css">Link CSS</h3>
	<p>Link your css file in between your head tags and we will also add a <a href="https://fonts.google.com/specimen/Roboto?selection.family=Roboto" target="_blank">Google Font</a>.</p>
<figure class="highlight"><pre><code class="language-html" data-lang="html"><span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">"stylesheet"</span> <span class="na">href=</span><span class="s">"https://fonts.googleapis.com/css?family=Roboto"</span><span class="nt">&gt;</span>
<span class="nt">&lt;link</span> <span class="na">rel=</span><span class="s">"stylesheet"</span> <span class="na">href=</span><span class="s">"assets/css/main.css"</span><span class="nt">&gt;</span></code></pre></figure>

	<h2 id="add-css">Add CSS</h2>
	<p>Let's add a bit of styling to our header and card.</p>
<figure class="highlight"><pre><code class="language-css" data-lang="css">
<span class="s">body</span> <span class="p">{</span>
  <span class="nl">font-family</span><span class="p">:</span> <span class="n">"Roboto", sans-serif</span><span class="p">;</span>
<span class="p">}</span>
<span class="s">h1</span> <span class="p">{</span>
  <span class="nl">color</span><span class="p">:</span> <span class="n">#2196F3</span><span class="p">;</span>
  <span class="nl">font-size</span><span class="p">:</span> <span class="n">40px</span><span class="p">;</span>
<span class="p">}</span>
<span class="nc">.card</span> <span class="p">{</span>
  <span class="nl">box-shadow</span><span class="p">:</span> <span class="n">0 4px 8px 0 rgba(0,0,0,0.2)</span><span class="p">;</span>
  <span class="nl">transition</span><span class="p">:</span> <span class="n">0.3s</span><span class="p">;</span>
  <span class="nl">width</span><span class="p">:</span> <span class="n">40%;</span><span class="p">;</span>
  <span class="nl">border-radius</span><span class="p">:</span> <span class="n">5px</span><span class="p">;</span>
<span class="p">}</span>
<span class="nc">.card</span><span class="nt">:hover</span> <span class="p">{</span>
  <span class="nl">box-shadow</span><span class="p">:</span> <span class="n">0 8px 16px 0 rgba(0,0,0,0.2)</span><span class="p">;</span>
<span class="p">}</span>
<span class="s">img</span> <span class="p">{</span>
  <span class="nl">border-radius</span><span class="p">:</span> <span class="n">5px 5px 0 0</span><span class="p">;</span>
<span class="p">}</span>
<span class="nc">.container</span> <span class="p">{</span>
  <span class="nl">padding</span><span class="p">:</span> <span class="n">2px 16px</span><span class="p">;</span>
<span class="p">}</span>
</code></pre></figure>
	<h3>Our Website is Done.</h3>
	<p>Time to host it.</p>
	
	<h2 id="host-web">Hosting Website</h2>
	<p>Now that we have created the website it is time to use it using <a href="https://www.ubuntu.com/">Apache</a>.</p>
	<h3 id="prep-ubuntu">Preparing Ubuntu</h3>
	<p>After installing <a href="https://www.ubuntu.com/">Ubuntu</a> on a virtual machine. We have to install apache. <a href="https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-18-04-quickstart">Full Tutorial.</a></p>
<figure class="highlight"><pre><code class="language-js" data-lang="js"><span class="k">sudo</span> <span class="s1">apt update</span>
<span class="k">sudo</span> <span class="s1">apt install apache2</span>
</code></pre></figure>
	<h3 id="add-site">Adding Website to Host</h3>
	<p>After installing apache you will be able to see that startpage at <a href="http://localhost/">localhost</a> (if it is installed on your computer).</p>
	<p>Copy all the files in the project folder into the htdocs folder in apache.</p>
	<a href="/project/index.php" id="final"><h2>Final Website</h2></a>
	
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/popper.min.js"></script>
	<script src="https://getbootstrap.com/docs/4.1/dist/js/bootstrap.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/docsearch.js@2/dist/cdn/docsearch.min.js"></script>
	<script src="https://getbootstrap.com/docs/4.1/assets/js/docs.min.js"></script>
</body>